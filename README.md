# os-bootstrap-workstation
- getting a macos or such workstation up with tools like supernova and bash aliases.

# running mojave base, needed some extra commands for git, virtualenvs...
```
mkdir -p ~/git/forks
cd !$
xcode-select --install
brew install git
brew doctor --debug --verbose

# brew doctor reported these needed to be run:
sudo mkdir -p /usr/local/include /usr/local/lib /usr/local/opt /usr/local/sbin /usr/local/Cellar
sudo chown -R $(whoami) /usr/local/include /usr/local/lib /usr/local/opt /usr/local/sbin /usr/local/Cellar
brew install git
git clone git@gitlab.com:okeddie/os-bootstrap-workstation.git

mkdir ~/venvs
cd !$
sudo easy_install pip
sudo pip install virtualenv

```

# hostname
```
Macs-Air:~ eddie.vasquez$ sudo scutil --set HostName devops
Macs-Air:~ eddie.vasquez$ sudo scutil --set ComputerName devops

```
