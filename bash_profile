# ALIASES
alias ll='ls -lh'
alias kubectl='~/git/forks/k8s-artifacts/kubectl-1.10.5'
alias ktx='kubectx'
alias pgen="pwgen -c -n -y -r'!' -B 20 5 -1 |tee >> .pgen_password_history ; tail .pgen_password_history"
alias nas="ssh eddie@192.168.0.3"
alias osl='openstack server list --os-cloud=os-admin --all-projects --long -c ID -c Host -c Name -c Networks -c "Image Name"'
alias squashy='git commit -m sq ; git rebase -i origin/master'
alias grep='grep --color'
alias gvms='gcloud compute instances list --format="yaml(hostname)"'

# EXPORTS AND VARS
export PS1="[\u@\h \W]\\$ "
export BASH_SILENCE_DEPRECATION_WARNING=1
export HISTFILESIZE=1000000
export HISTSIZE=1000000
export ANSIBLE_ACTION_WARNINGS=False
export ANSIBLE_DEPRECATION_WARNINGS=False
export ANSIBLE_PYTHON_INTERPRETER=auto_silent
export ANSIBLE_DUPLICATE_YAML_DICT_KEY=ignore
export MINICOM="-m -c on"

# You also need to edit System Preferences > Keyboard > Shortcuts > Mission Control > Uncheck ctrl+L/R
bind '"\e[5C": forward-word'
bind '"\e[5D": backward-word'
bind '"\e[1;5C": forward-word'
bind '"\e[1;5D": backward-word'

# FUNCTIONS
# scripted addition of hosts for java security issues.
function drac-ie() {
SITE=`grep $1 /Users/$USER/Library/Application\ Support/Oracle/Java/Deployment/security/exception.sites`
HTTP=`echo $1 | egrep "http(s?)"`
if [ -z "$SITE" -a -n "$HTTP" ]; then
  echo $1 >> /Users/$USER/Library/Application\ Support/Oracle/Java/Deployment/security/exception.sites
  echo "Added $1 to the exception list"
elif [ -z "$HTTP" ]; then
  echo "Site $1 doesn't have http or https, not added"
else
  echo "Site $1 is already on the list"
fi
}

# add script to fetch upstream master/merge
function fup() {
git checkout master
git fetch upstream
git merge upstream/master
git push origin master
}

# pull a gif/meme easily from string
function i() {
STRING=$1
grep -A1 $STRING ~/pso/imgur
}

# inject an image url into github autoexpanded comment.
function gimage() {
echo "![alt text]($@)" |pbcopy
}

# kill the macbook touch bar. (when it gets stuck on max volume, you'll want this.)
function fbar() {
sudo kill -9 $(sudo ps -A |grep [T]ouchBarServer |awk '{print $1}')
}

# delete line of offending ssh public keys.
function unrsa() {
LINE_NUM=$1
/usr/bin/sed -i.orig -e "${LINE_NUM}d" ~/.ssh/known_hosts
}

### dynamic vault usage functions ###
# vault list.
function vlist(){
export VAULT_ADDR='http://127.0.0.1:8200'
ln -svf ~/.vault-token.local ~/.vault-token
vault kv list secret
export VAULT_ADDR="https://$HOST_NAME:$PORT"
ln -svf ~/.vault-token.rackyrack ~/.vault-token
}

# vault get credentials
function vget() {
export VAULT_ADDR='http://127.0.0.1:8200'
ln -svf ~/.vault-token.local ~/.vault-token
SECRET_OBJECT=$1
vault kv get secret/$SECRET_OBJECT
export VAULT_ADDR="https://$HOST_NAME:$PORT"
ln -svf ~/.vault-token.rackyrack ~/.vault-token
}

# vault put credentials
function vput() {
export VAULT_ADDR='http://127.0.0.1:8200'
ln -svf ~/.vault-token.local ~/.vault-token
vault kv put secret/$@
export VAULT_ADDR="https://$HOST_NAME:$PORT"
ln -svf ~/.vault-token.rackyrack ~/.vault-token
}

# vault put credentials
function vdel() {
export VAULT_ADDR='http://127.0.0.1:8200'
ln -svf ~/.vault-token.local ~/.vault-token
vault kv delete secret/$@
export VAULT_ADDR="https://$HOST_NAME:$PORT"
ln -svf ~/.vault-token.rackyrack ~/.vault-token
}

# vault append credentials
function vadd() {
export VAULT_ADDR='http://127.0.0.1:8200'
ln -svf ~/.vault-token.local ~/.vault-token
SECRET_OBJECT=$1
vault kv patch secret/$@
export VAULT_ADDR="https://$HOST_NAME:$PORT"
ln -svf ~/.vault-token.rackyrack ~/.vault-token
}

# Virtual env functions
# Note some functions require quoting args or else an arg with quotes is stripped of quotes.
#function ansible() {
#source $HOME/venvs/ansible2.x/bin/activate
#$HOME/venvs/ansible2.x/bin/ansible "$@"
#deactivate
#}
#
#function ansible-playbook() {
#source $HOME/venvs/ansible2.x/bin/activate
#$HOME/venvs/ansible2.x/bin/ansible-playbook "$@"
#deactivate
#}

function ansible28() {
source $HOME/venvs/ansible2.8/bin/activate
$HOME/venvs/ansible2.8/bin/ansible "$@"
deactivate
}

function openstack() {
source $HOME/venvs/openstack/bin/activate
$HOME/venvs/openstack/bin/openstack "$@"
deactivate
}

